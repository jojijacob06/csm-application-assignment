#include "utilities.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int listen_for_updates(zsock_t *sub)
{
  char *recv_string = NULL;
  int seq_number = -1;
  recv_string = zstr_recv(sub);
  if (!recv_string)
  {
    zsys_error("Interrupted!!!");
    return -1;
  }
  zsys_info("Subscriber received: %s", recv_string);
  
  seq_number = get_value_of(recv_string, "seq_number");
  zstr_free(&recv_string);
  
  return seq_number;
}

int push_image_response(int seq_number, int image_size, zsock_t *push)
{
  int push_result = 0;
  char message[100];
  
  sprintf(message, ">seq_number=%d,>image_size=%d", seq_number, image_size);
  zsys_info("Cubesat sending message: %s", message);
  push_result = zstr_send(push, message);
  
  return push_result;
}

int main (int argc, char *argv[])
{
  zsock_t *sub = NULL;
  zsock_t *push = NULL;
  
  if (argc != 2)
  {
    printf("Usage: %s <port_number>\n", argv[0]);
    exit(1);
  }
  
  init_logging("Cubesat");
  
  srand((unsigned)time(NULL));
  
  sub = create_subscriber(7777);
  push = create_push_socket(atoi(argv[1]));  
  
  while (1)
  {
    int seq_number = listen_for_updates(sub);
    if (seq_number == -1)
    {
      zsys_error("Error in receiving subscription: %s", strerror(zmq_errno()));
      break;
    }
    
    if (push_image_response(seq_number, rand()%1000, push) == -1)
    {
      zsys_error("Error in pushing image response: %s", strerror(zmq_errno()));
      break;
    }
  }
 
  zsock_destroy(&push); 
  zsock_destroy(&sub);
  return 0;
}
