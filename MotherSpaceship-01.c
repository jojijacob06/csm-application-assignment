#include <czmq.h>

#include <string.h>

#include <stdio.h>

void init_logging()
{
  zsys_set_logident("MotherSpaceship");
}

zsock_t * create_publisher(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://*:%d", port_number);
  zsys_info("Creating publisher at %s", endpoint);
  zsock_t *pub = zsock_new_pub(endpoint);
  if (!pub)
  {
    zsys_error("Error in creating publisher: %s", strerror(zmq_errno()));
    exit(1);
  }
  return pub;
}

zsock_t * create_stream_socket(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://*:%d", port_number);
  zsys_info("Creating stream socket at: %s", endpoint);
  zsock_t *stream = zsock_new_stream(endpoint);
  if(!stream)
  {
    zsys_error("Error in creating stream socket: %s", strerror(zmq_errno()));
    exit(1);
  }
  return stream;
}

zpoller_t * create_poller(zsock_t *socket_to_wait_on)
{
  zsys_info("Creating poller");
  zpoller_t *poller = zpoller_new(socket_to_wait_on);
  if(!poller)
  {
    zsys_error("Error in creating poller: %s", strerror(zmq_errno()));
    exit(1);
  }
  return poller;
}

int publish_capture_command(zsock_t *pub, int sequence_number)
{
  int publish_result = 0;
  
  zsys_info("Publishing capture command with sequence number (%d)", sequence_number);
  publish_result = zstr_sendf(pub, "Image capture command 0x%4x", sequence_number);
  
  return publish_result;
}

int main (void)
{
  zsock_t *pub = NULL;
  zsock_t *stream = NULL;
  zpoller_t *poller = NULL;
  int64_t global_timeout = 2000;
  int64_t start_time = 0;
  
  init_logging();
  
  pub = create_publisher(7777);
  stream = create_stream_socket(8888);
  poller = create_poller(stream);  
  
  int sequence_number = 0;
  while (sequence_number++ < 10)
  {
    if (publish_capture_command(pub, sequence_number) == -1)
    {
      zsys_error("Error in publishing: %s", strerror(zmq_errno()));
      break;
    }   

    start_time = zclock_mono();
    
    while(zclock_mono()-start_time < global_timeout)
    {
      int timeout = global_timeout - (zclock_mono() - start_time);
      zsys_info("Setting timeout of %d milli secs for poller", timeout);
      zsock_t *which = zpoller_wait(poller, timeout);
      if(which == stream)
      {
        char *picture = zstr_recv(stream);
        zsys_info("Mother space ship received: %s", picture);
        zstr_free(&picture);
      }
    }    
  }
  
  zpoller_destroy(&poller);
  zsock_destroy(&stream);
  zsock_destroy(&pub);
  
  return 0;
}
