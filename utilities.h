#include <czmq.h>

#include <stdlib.h>

void init_logging(const char *identity)
{
  zsys_set_logident(identity);
}

zsock_t * create_publisher(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://*:%d", port_number);
  zsys_info("Creating publisher at %s", endpoint);
  zsock_t *pub = zsock_new_pub(endpoint);
  if (!pub)
  {
    zsys_error("Error in creating publisher: %s", strerror(zmq_errno()));
    exit(1);
  }
  return pub;
}

zsock_t * create_subscriber(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://localhost:%d", port_number);
  zsys_info("Creating subscriber at %s", endpoint);
  zsock_t *sub = zsock_new_sub(endpoint, "");
  if (!sub)
  {
    zsys_error("Error in creating subscriber: %s", strerror(zmq_errno()));
    exit(1);
  }
  return sub;
}

zpoller_t * create_poller(zsock_t **sockets_to_wait_on, int count)
{
  zsys_info("Creating poller");
  zpoller_t *poller = zpoller_new(sockets_to_wait_on[0]);
  if(!poller)
  {
    zsys_error("Error in creating poller: %s", strerror(zmq_errno()));
    exit(1);
  }
  int i=0;
  for ( ; i<count; i++)
  {
    if (zpoller_add(poller, sockets_to_wait_on[i]) == -1)
    {
      zsys_error("Error in creating poller: %s", strerror(zmq_errno()));
      exit(1);
    }
  }
  return poller;
}

zsock_t * create_pull_socket(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://*:%d", port_number);
  zsys_info("Creating pull socket at %s", endpoint);
  zsock_t *pull = zsock_new_pull(endpoint);
  if(!pull)
  {
    zsys_error("Error in creating pull socket: %s", strerror(zmq_errno()));
    exit(1);
  }
  return pull;
}

zsock_t * create_push_socket(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://localhost:%d", port_number);
  zsys_info("Creating push socket at %s", endpoint);
  zsock_t *push = zsock_new_push(endpoint);
  if (!push)
  {
    zsys_error("Error in creating push socket: %s", strerror(zmq_errno()));
    exit(1);
  }
  return push;
}

int get_value_of(char *haystack, char *needle)
{
  char *haystack_itr = haystack;
  char *needle_itr = needle;
  
  int value = -1;
  
  while (*haystack_itr && *needle_itr)
  {
    while (*haystack_itr != '>')
    {
      haystack_itr++;
    }
    haystack_itr++;
    
    while (*haystack_itr && *needle_itr && *haystack_itr == *needle_itr)
    {
      *haystack_itr++;
      *needle_itr++;
    }
    
    if (*needle_itr)
    {
      needle_itr = needle;
      continue;
    }
    else 
    {
      haystack_itr++;
      char string_value[100];
      int i=0;
      while (*haystack_itr && *haystack_itr != ',')
      {
        string_value[i++] = *haystack_itr;
        haystack_itr++;
      }
      string_value[i] = '\0';
      value = atoi(string_value);
      break;
    }
  }
  
  return value;
}
