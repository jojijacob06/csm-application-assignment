#include "utilities.h"

#include <stdio.h>
#include <math.h>

#define MAX_NUMBER_OF_CUBESATS  10
#define PULL_SOCKET_PORT_START  8001

int publish_capture_command(zsock_t *pub, int sequence_number)
{
  int publish_result = 0;
  
  zsys_info("Publishing capture command with sequence number (%d)", sequence_number);
  publish_result = zstr_sendf(pub, ">seq_number=%d", sequence_number);
  
  return publish_result;
}

int main (int argc, char *argv[])
{
  zsock_t *pub = NULL;
  zsock_t *pull[MAX_NUMBER_OF_CUBESATS] = {NULL, };
  zpoller_t *poller = NULL;
  int image_size[MAX_NUMBER_OF_CUBESATS]= {0, };
  int number_cubesats=0;
  int sequence_number=0;
  int number_image_responses=0;
  int64_t global_timeout = 10000;
  int64_t start_time = 0;
  int i=0;
  
  if (argc == 1)
  {
    printf("Usage: %s <number_of_cubesats>\n", argv[0]);
    exit(1);
  }
    
  number_cubesats = atoi(argv[1]);
  if (number_cubesats < 1 || number_cubesats > 10)
  {
    printf("This version supports a maximum of 10 cubesats.");
    exit(1);
  }

  init_logging("MotherSpaceship");

  pub = create_publisher(7777);
  for (i=0; i<number_cubesats; i++)
  {
    pull[i] = create_pull_socket(PULL_SOCKET_PORT_START+i);
  }
  poller = create_poller(pull, number_cubesats);
  
  while (sequence_number++ < 20)
  {
    if (publish_capture_command(pub, sequence_number) == -1)
    {
      zsys_error("Error in publishing: %s", strerror(zmq_errno()));
      break;
    }   

    start_time = zclock_mono();
    
    number_image_responses = 0;
    while(zclock_mono()-start_time < global_timeout)
    {
      int timeout = global_timeout - (zclock_mono() - start_time);
      zsock_t *which = zpoller_wait(poller, timeout);
      if(which)
      {
        char *picture = zstr_recv(which);
        if (sequence_number != get_value_of(picture, "seq_number"))
        {
          zsys_info("Wrong sequence number (%d); ignored!!", get_value_of(picture, "seq_number"));
        }
        else
        {
          image_size[number_image_responses] = get_value_of(picture, "image_size");
          zsys_info("Image size received on %s: %d", zsock_endpoint(which), image_size[number_image_responses]);
        }
        number_image_responses++;
        zstr_free(&picture);
      }
    }
    if (number_image_responses == 2)
    {
      zsys_info("Difference of image sizes received: %d", abs(image_size[0]-image_size[1]));
    }
    else
    {
      //do something else!!!
    }  
  }
  
  zpoller_destroy(&poller);
  for (i=0; i<number_cubesats; i++)
  {
    zsock_destroy(&pull[i]);
  }
  zsock_destroy(&pub);
  
  return 0;
}
