#include <czmq.h>

void init_logging()
{
  zsys_set_logident("Cubesat");
}

zsock_t * create_subscriber(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://localhost:%d", port_number);
  zsys_info("Creating subscriber at %s", endpoint);
  zsock_t *sub = zsock_new_sub(endpoint, "");
  if (!sub)
  {
    zsys_error("Error in creating subscriber: %s", strerror(zmq_errno()));
    exit(1);
  }
  return sub;
}

zsock_t * create_stream_socket(int port_number)
{
  char endpoint[100];
  sprintf(endpoint, "tcp://*:%d", port_number);
  zsys_info("Creating stream socket at: %s", endpoint);
  zsock_t *stream = zsock_new_stream(endpoint);
  if(!stream)
  {
    zsys_error("Error in creating stream socket: %s", strerror(zmq_errno()));
    exit(1);
  }
  return stream;
}

void listen_for_updates(zsock_t *sub)
{
  char *recv_string = NULL;
  recv_string = zstr_recv(sub);
  if (!recv_string)
  {
    zsys_error("Interrupted!!!");
    return;
  }
  zsys_info("Subscriber received: %s", recv_string);
  zstr_free(&recv_string);
}

int send_image_response(zsock_t *stream_socket)
{
  int send_result = 0;
  
  char *message = "A new image from cubesat";
  zsys_info("Pushing a new image: %s", message);
  send_result = zstr_send(stream_socket, message);
  
  return send_result;
}

int main (void)
{
  zsock_t *sub = NULL;
  xsock_t *stream = NULL;
  
  init_logging();
  
  sub = create_subscriber(7777);
  stream = create_stream_socket(8888);
  
  int counter = 0;
  
  while (counter++ < 5)
  {
    listen_for_updates(sub);
    
    if(send_image_response(stream, counter) == -1)
    {
      zsys_error("Error in sending: %s", strerror(zmq_errno()));
      break;
    }
  }
 
  zsock_destroy(&stream); 
  zsock_destroy(&sub);
  return 0;
}
